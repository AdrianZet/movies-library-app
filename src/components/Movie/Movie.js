class Movie {
  constructor(title, year, runtime, actors, awards, genre, director, country, language, 
              plot, poster, production, rating, website, writer, imdbRating, isWatched, id) {

    if ( !(title) ) {
      throw new Error("Corect movie title is required!");
    }

		this.title = title;
    this.year = year;
    this.runtime = runtime;
    this.actors = actors;
    this.awards = awards;
    this.genre = genre;
    this.director = director;
    this.country = country;
    this.language = language;
    this.plot = plot;
    this.poster = poster;
    this.production = production;
    this.rating = rating;
    this.website = website;
    this.writer = writer;
    this.imdbRating = imdbRating;
    this.isWatched = isWatched;
    this.id = id;
  }
}

export default Movie;