const SaveData = (library) => {
  localStorage.setItem('Library', JSON.stringify(library));
}

export default SaveData;