import GetData from '../GetData/GetData.js';
import SaveData from '../SaveData/SaveData.js';
import Library from '../../Library/Library.js';
import RenderList from '../../../containers/Interface/RenderList/RenderList.js';


const DeleteData = () => {
  let library = new Library();
  GetData(library);

  let $deleteBtn = document.querySelectorAll('.removedBtn');

  $deleteBtn.forEach(function(item) {
    item.addEventListener('click', function(event) {
      let movieID = item.parentNode

      for (let i = 0; i < library.movies.length; i++) {
        if (library.movies[i].id === movieID.id) {
          library.movies.splice(i, 1);
          movieID.remove();
          SaveData(library);
          //location.reload();
        }
      }
      return library;
    })
  });
};

export default DeleteData;