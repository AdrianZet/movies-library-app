const GetData = (library) => {
  let libraryInStorage = localStorage.getItem('Library');

  if(libraryInStorage === null || libraryInStorage === '{"movies":[]}') {
    console.log('Library is empty')
  } else {
    let moviesArr = (JSON.parse(libraryInStorage)).movies;
    moviesArr.forEach(function(item) {
      library.add(item);
    });   
  }
}

export default GetData;