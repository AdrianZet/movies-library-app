import axios from 'axios';
import Movie from '../Movie/Movie.js';
import GetData from '../LocalStorage/GetData/GetData.js';
import SaveData from '../LocalStorage/SaveData/SaveData.js';
import Library from '../Library/Library.js';
import RenderList from '../../containers/Interface/RenderList/RenderList.js';
import DeleteData from '../LocalStorage/DeleteData/DeleteData.js';

let library = new Library();

GetData(library);

library.movies.forEach(function(item) {
  RenderList(item);
});

const FetchData = (movieTitle) => {

  let $appContainer = document.getElementById('App');

  if(movieTitle) {
    axios.get('http://www.omdbapi.com/?t=' + movieTitle + '&apikey=a6be961d')
      .then(response => {
        let $movieIsWatched = document.getElementById('checkbox-1').checked;
        let $setMovieInfo = document.createElement('div');
        let movie;

        $movieIsWatched = $movieIsWatched ? 'Yes' : 'No';
        $setMovieInfo.classList.add('row');

        movie = new Movie(response.data.Title, response.data.Year, response.data.Runtime,
                              response.data.Actors, response.data.Awards, response.data.Genre,
                              response.data.Director, response.data.Country, response.data.Language,
                              response.data.Plot, response.data.Poster, response.data.Production,
                              response.data.Rating, response.data.Website, response.data.Writer,
                              response.data.imdbRating, $movieIsWatched, response.data.imdbID);
    
        UpdateInterface(movie);
        DeleteData();
      });
  }
  
  const UpdateInterface = (movie) => {
    let isExists = library.movies.find(function(element) {
        if (element.title === movie.title){
          return true;
        }
    }); 

    if (!isExists) {
      library.add(movie);
      SaveData(library);
      RenderList(movie);
    } else {
      console.log('Movie already exist in library');
    }
  };
}
export default FetchData;