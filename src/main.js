import Movie from './components/Movie/Movie.js';
import Report from './components/Report/Report.js';
import Interface from './containers/Interface/Interface.js';
import RenderSearch from './containers/Interface/RenderSearch/RenderSearch.js';

require('./assets/style/main.scss');

window.onload = function() {

  setTimeout(function() {
    document.getElementById('App-container').style.display = 'block'; 
    document.querySelector('.show-list').style.display = 'block';
    document.querySelectorAll('.movie-item').forEach(function (el) {
      el.style.display = 'flex';
    });
    document.querySelector('body').className = 'loaded';  
  }, 2000);

  setTimeout(function() { 
    document.getElementById('loader-wrapper').style.display = 'none';  
  }, 3800);

  RenderSearch();

  try {
    var movie = new Movie("test", false);
    var report = new Report();
    
    report.getDebugInfo(movie);
  } catch(e) {
    console.log(e.message);
  }

  Interface();
};