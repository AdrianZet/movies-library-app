import FetchData from '../../components/FetchData/FetchData.js';
import DeleteData from '../../components/LocalStorage/DeleteData/DeleteData.js';

const Interface = () => {
  let $search = document.getElementById('movie-title');

  document.getElementById('movie-title').addEventListener('focus', function (event) {
    $search.value = '';
  });

  document.getElementById('movie-title').addEventListener('keypress', function (event) {
    let key = event.witch || event.keyCode;
    if (key === 13) {
      let movieTitle = $search.value.split(' ').join('+'); 
      FetchData(movieTitle);
      $search.value = '';
    }
  });

  document.getElementById('add-movie').addEventListener('click', function (event) {
    let movieTitle = $search.value.split(' ').join('+'); 
    FetchData(movieTitle);
    $search.value = '';
  });

  DeleteData();
}

export default Interface;


