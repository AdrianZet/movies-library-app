import FetchData from '../../../components/FetchData/FetchData';
import cover from '../../../assets/img/cover.gif';

const RenderList = (item) => {
  let $appContainer = document.getElementById('App');
  let $setMovieInfo = document.createElement('div');
  let $layer = document.createElement('div')

  $setMovieInfo.classList.add('movie-item');
  //$setMovieInfo.style.display = 'none';
  $setMovieInfo.setAttribute('id', item.id);
  $layer.classList.add('movie-item__layer');
  $setMovieInfo.appendChild($layer);

  (item.poster == 'N/A') ? item.poster = '"' + cover + '"' : item.poster = item.poster;
  (item.plot == 'N/A') ? item.plot = "We couldn't find the information. Sorry ..." : item.plot = item.plot;

  $setMovieInfo.innerHTML += '<div class="movie-item__poster">' +
                              '<img src=' + item.poster + 'class="movie-poster" alt="' + item.title + '">' +
                            '</div>' +
                            '<div class="movie-item__description">' + 
                              '<h4>' 
                                + item.title + ' ' + 
                                '(' + item.year + ')' +
                              '</h4>' + 
                              '<div>' + 
                                '<p>' + item.plot + '</p>' +
                                '<ul>' +
                                  '<li>' + '<b>Director</b>: ' + item.director + '</li>' +
                                  '<li>' + '<b>Writer</b>: ' + item.writer + '</li>' +
                                  '<li>' + '<b>Country</b>: ' + item.country + '</li>' +
                                  '<li>' + '<b>Actors</b>: ' + item.actors + '</li>' +
                                  '<li>' + '<b>Production</b>: ' + item.production + '</li>' +
                                '</ul>' +
                                '<ul>' +
                                  '<li>' + '<b>Awards</b>: ' + item.awards + '</li>' +
                                  '<li>' + '<b>Rating</b>: ' + item.imdbRating + '</li>' +
                                '</ul>' +
                              '</div>' + 
                            '</div>' + 
                            '<button type="button" class="removedBtn">delete</button>' ; 
  $appContainer.insertBefore($setMovieInfo, $appContainer.firstChild)
}

export default RenderList;